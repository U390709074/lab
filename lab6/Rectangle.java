public class Rectangle {

    int sideA;
    int sideB;
    Point topLeft;

    public Rectangle(int sideA, int sideB, Point topLeft) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.topLeft = topLeft;
    }

    public int area(){
        return sideA * sideB;
    }

    public int perimeter(){
        return 2 * (sideA + sideB);
    }

    public Point[] corners(){
        Point[] corners = new Point[4];
        corners[0] = topLeft;
        corners[1] = new Point(topLeft.xCrood + sideA, topLeft.yCrood);
        corners[2] = new Point(topLeft.xCrood , topLeft.yCrood-sideB);
        corners[3] = new Point(topLeft.xCrood + sideA, topLeft.yCrood-sideB);
        return corners;
    }
}
