public class Point {
    int xCrood;
    int yCrood;

    public Point(int xCrood , int yCrood){
        this.xCrood = xCrood;
        this.yCrood = yCrood;
    }

    public double distanceFromAPoint(Point point){
        int xDiff = xCrood - point.xCrood;
        int yDiff = yCrood - point.yCrood;
        return Math.sqrt(xDiff*xDiff + yDiff*yDiff);
    }

}
